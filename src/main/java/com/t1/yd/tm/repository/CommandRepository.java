package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.ICommandRepository;
import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;
import com.t1.yd.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(ArgumentConstant.HELP, CommandConstant.HELP, "Show info about program");
    private static final Command ABOUT = new Command(ArgumentConstant.ABOUT, CommandConstant.ABOUT, "Show info about program");
    private static final Command VERSION = new Command(ArgumentConstant.VERSION, CommandConstant.VERSION, "Show program version");
    private static final Command INFO = new Command(ArgumentConstant.INFO, CommandConstant.INFO, "Show system info");
    private static final Command COMMANDS = new Command(ArgumentConstant.COMMANDS, CommandConstant.COMMANDS, "Show available commands");
    private static final Command ARGUMENTS = new Command(ArgumentConstant.ARGUMENTS, CommandConstant.ARGUMENTS, "Show available arguments");

    private static final Command PROJECT_CREATE = new Command(null, CommandConstant.PROJECT_CREATE, "Create new project");
    private static final Command PROJECT_LIST = new Command(null, CommandConstant.PROJECT_LIST, "Show all projects");
    private static final Command PROJECT_CLEAR = new Command(null, CommandConstant.PROJECT_CLEAR, "Clear all projects");

    private static final Command TASK_CREATE = new Command(null, CommandConstant.TASK_CREATE, "Create new task");
    private static final Command TASK_LIST = new Command(null, CommandConstant.TASK_LIST, "List all tasks");
    private static final Command TASK_CLEAR = new Command(null, CommandConstant.TASK_CLEAR, "Clear all tasks");

    private static final Command EXIT = new Command(null, CommandConstant.EXIT, "Exit program");

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, TASK_CREATE, TASK_LIST, TASK_CLEAR, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
