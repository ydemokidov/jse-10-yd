package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.ITaskRepository;
import com.t1.yd.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }
}
