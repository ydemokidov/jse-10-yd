package com.t1.yd.tm.service;

import com.t1.yd.tm.api.ITaskRepository;
import com.t1.yd.tm.api.ITaskService;
import com.t1.yd.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return add(new Task(name));
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task add(Task task) {
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }
}
