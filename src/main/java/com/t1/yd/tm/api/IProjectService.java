package com.t1.yd.tm.api;

import com.t1.yd.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void clear();

    Project create(final String name);

    Project create(final String name, final String description);

    List<Project> findAll();

    Project add(Project project);

}
