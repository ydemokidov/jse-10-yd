package com.t1.yd.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
