package com.t1.yd.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
