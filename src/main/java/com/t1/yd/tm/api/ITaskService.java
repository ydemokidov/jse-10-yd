package com.t1.yd.tm.api;

import com.t1.yd.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    void clear();

    Task add(Task task);

    List<Task> findAll();

}
