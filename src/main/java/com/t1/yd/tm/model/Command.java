package com.t1.yd.tm.model;

public class Command {

    private String argument;
    private String name;
    private String description;

    public Command() {
    }

    public Command(String argument, String name, String description) {
        this.argument = argument;
        this.name = name;
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("%s%s : %s", name, argument != null ? ", " + argument : "", description);
    }
}
