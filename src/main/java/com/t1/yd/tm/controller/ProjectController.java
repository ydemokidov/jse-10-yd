package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.IProjectController;
import com.t1.yd.tm.api.IProjectService;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");

        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[ERROR]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();

        int index = 1;
        for (Project p : projects) {
            System.out.println(index + ". " + p);
            index++;
        }

        System.out.println("[OK]");
    }

}
