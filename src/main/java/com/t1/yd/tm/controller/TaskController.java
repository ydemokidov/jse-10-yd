package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.ITaskController;
import com.t1.yd.tm.api.ITaskService;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");

        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[ERROR]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        final List<Task> projects = taskService.findAll();

        int index = 1;
        for (Task p : projects) {
            System.out.println(index + ". " + p);
            index++;
        }

        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }
}
